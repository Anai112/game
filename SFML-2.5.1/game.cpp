#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <ctime>
#include <string>
#include <cstdlib>

using namespace sf;

using std::to_string;

struct mina
{
    bool flag;

    int va;

};

int main_menu(RenderWindow& window, bool flag=true, int point=0)
{
    static Font font;
    font.loadFromFile("./src/font/font.ttf");
    static Text text1("",font,30);
    static Text text2("",font,30);
    static Text text3("",font,30);
    static Event e;
    static Texture background;
    static Sprite background_s;
    background_s.setTextureRect(IntRect(0,0,1200,1195));
    background_s.setPosition(0,0);
    background_s.setTexture(background);
    background_s.setScale(window.getSize().x/background_s.getLocalBounds().width,window.getSize().x/background_s.getLocalBounds().height);
    background.loadFromFile("./src/image/back.jpeg");
    text1.setString("ARCADE");
    text2.setString("TIME ATTACK");
    text3.setString("EXIT");
    text1.setColor(Color::Magenta);
    text2.setColor(Color::Magenta);
    text3.setColor(Color::Magenta);
    text1.setPosition(window.getSize().x/2-80.f,window.getSize().y/2.f-40);
    text2.setPosition(text1.getPosition().x,text1.getPosition().y+40);
    text3.setPosition(text2.getPosition().x,text2.getPosition().y+40);
    
    if(flag)
    {
        while(window.isOpen())
        {
            while(window.pollEvent(e))
            {
                if(e.type == sf::Event::Closed)
                    return 0;

                if(IntRect(text1.getPosition().x,text1.getPosition().y,180,30).contains(Mouse::getPosition(window))&& Mouse::isButtonPressed(Mouse::Left)){ return 1;}
                if(IntRect(text2.getPosition().x,text2.getPosition().y,330,30).contains(Mouse::getPosition(window)) && Mouse::isButtonPressed(Mouse::Left)){ return 2;}
                if(IntRect(text3.getPosition().x,text3.getPosition().y,90,30).contains(Mouse::getPosition(window)) && Mouse::isButtonPressed(Mouse::Left)){ return 0;}
                window.clear();
                window.draw(background_s);
                window.draw(text1);
                window.draw(text2);
                window.draw(text3);
                window.display();
            }
        }
    }
    
    else
    {
        while(window.isOpen())
        {
            while(window.pollEvent(e))
            {
                if(e.type == Event::Closed)
                    return 0;
                
                if(IntRect(text3.getPosition().x,text3.getPosition().y,90,30).contains(Mouse::getPosition(window)) && Mouse::isButtonPressed(Mouse::Left)){return 0;}
                
                text1.setString("You deactivated "+to_string(point)+" mines");

                text1.setPosition(window.getSize().x/2-190.f,window.getSize().y/2.f-20);

                window.clear();

                window.draw(background_s);

                window.draw(text3);

                window.draw(text1);
                
                window.display();
            }
        }
    }
}

int main()
{    
    RenderWindow window(VideoMode(640, 640), "Techies Tactics",Style::Close);

    int mode = main_menu(window,true);

    int min_count = 0;

    int point = 0;

    Font font;

    font.loadFromFile("./src/font/font.ttf");

    Text _min_count(Text("",font,20));

    Text flag_count_("",font,20);

    Text timer(Text("",font,20));

    timer.setColor(Color::White);

    flag_count_.setColor(Color::White);

    _min_count.setColor(Color::White);

    _min_count.setPosition(32,0);

    timer.setPosition(_min_count.getPosition().x+495,0);

    if(mode==1)
        flag_count_.setPosition(timer.getPosition().x-50,timer.getPosition().y);

    else
        flag_count_.setPosition(_min_count.getPosition().x+250, _min_count.getPosition().y);

    Time game_timer_ = seconds(30000.f);

    timer.setString("time :"+to_string(int(game_timer_.asSeconds()/1000.f)));
    
    if(mode==1 || mode==2)
    {
        srand(time(0));

        Texture t;
    
        t.loadFromFile("./src/image/map.jpeg");

        Sprite s(t);

        Event e;

        int x,y,n,w=32,size = 20,flag_count = mode==1?20:15;      

        mina** gridLogic = new mina*[size];

        for(int i = 0; i<size ; ++i)
            gridLogic[i] = new mina[size];

        int** gridView = new int*[size];

        for(int i=0 ;i<size ;++i)
            gridView[i] = new int[size];

        for(int i=0 ; i<size ; i++)
            for(int j=0 ; j<size ; ++j)
                gridView[i][j] = 10;

        for(int i=0 ; i<size ; i++){
            for(int j=0 ; j<size ; ++j)
            {
                gridLogic[i][j].va = 0;

                gridLogic[i][j].flag = false;
            }
        }

        for (int i = 1; i <= size-2; i++){
            for (int j = 1; j <= size-2; j++){
                if(rand()%5==0)
                {
                    gridLogic[i][j].va = 9;

                    min_count++;
                }
            }
        }
            

        for (int i = 1; i <= size-2; i++){
            for (int j = 1; j <= size-2; j++)
            {
                n = 0;
                
                if (gridLogic[i][j].va == 9) continue;
                
                if (gridLogic[i + 1][j].va== 9) n++;
                
                if (gridLogic[i][j + 1].va == 9) n++;
                
                if (gridLogic[i - 1][j].va == 9) n++;
                
                if (gridLogic[i][j-1].va == 9) n++;
                
                if (gridLogic[i + 1][j + 1].va == 9) n++;
                
                if (gridLogic[i - 1][j - 1].va == 9) n++;
                
                if (gridLogic[i - 1][j + 1].va == 9) n++;
                
                if (gridLogic[i + 1][j - 1].va == 9) n++;
                
                gridLogic[i][j].va = n;
            }
        }

        while(window.isOpen())
        {
            if(mode==2)
                game_timer_ = seconds(game_timer_.asSeconds()-0.1f);

            x = (Mouse::getPosition(window).x/w)%size;
            
            y = (Mouse::getPosition(window).y/w)%size;

            _min_count.setString("mines count "+to_string(min_count));
 
            while(window.pollEvent(e))
            {
                if (e.type == sf::Event::Closed)
                {
                    for(int i=0 ; i<size; i++)
                        delete[] gridLogic[i];

                    delete[] gridLogic;

                    for(int i=0 ; i<size; i++)
                        delete[] gridView[i];

                    delete[] gridView;

                    gridLogic = nullptr;

                    gridView = nullptr;

                    return 0;
                }
                
                if(e.type == Event::MouseButtonPressed)
                    if (e.key.code == Mouse::Left) gridView[x][y] = gridLogic[x][y].va;
                    else if (e.key.code == Mouse::Right && flag_count!=0)
                    {
                        gridView[x][y]=11;

                        flag_count--;

                        if(gridLogic[x][y].va==9 && gridLogic[x][y].flag==false)
                        {
                            min_count--;

                            point++;

                            flag_count+=2;

                            gridLogic[x][y].flag = true;
                        }
                    }
            }

            window.clear(Color::Black);

            for(int i = 1; i <= size-2; i++){
                for (int j = 1; j <= size-2; j++)
                {
                    if(gridView[x][y] == 9 || (mode==2 && game_timer_.asSeconds()<=0.f) || point == min_count)
                    {
                        gridView[i][j] = gridLogic[i][j].va;

                        for(int i=0 ; i<size; i++)
                            delete[] gridLogic[i];

                        delete[] gridLogic;

                        for(int i=0 ; i<size; i++)
                            delete[] gridView[i];

                        delete[] gridView;

                        gridLogic = nullptr;

                        gridView = nullptr;

                        goto quit;
                    }        
                    
                    s.setTextureRect(IntRect(gridView[i][j] * w, 0, w, w));

                    s.setPosition(i*w,j*w);
                
                    window.draw(s);
                }
            }

            if(mode==2)
            {
                timer.setString("time :"+to_string(int(game_timer_.asSeconds()/1000.f)));

                window.draw(timer);
            }

            flag_count_.setString("flag count "+to_string(flag_count));

            window.draw(flag_count_);

            window.draw(_min_count);
        
            window.display();
        }
    }

    else
        window.close();

    quit:
        main_menu(window,false,point);
}
